---
title: What is kdb
date: 2018-07-22T16:18:14.000Z
---
The [Wikipedia definition of kdb](https://en.wikipedia.org/wiki/Kdb%2B):

> kdb+ is a column-based relational time series database (TSDB) with in-memory (IMDB) abilities, developed and marketed by Kx Systems. The database is commonly used in high-frequency trading (HFT) to store, analyze, process, and retrieve large data sets at high speed. kdb+ has the ability to handle billions of records and analyzes data within a database. The database is available in 32-bit and 64-bit versions for several operating systems. Financial institutions use kdb+ to analyze time series data such as stock or commodity exchange data. The database has also been used for other time-sensitive data applications including commodity markets such as energy trading, telecommunications, sensor data, log data, and machine and computer network usage monitoring.

A oversimplified version of that paragraph is that kdb is just another database. It is not your usual Oracle database though -- it does not follow the relational data model, and instead is optimized for lists. Perfect for time series data, such as stock exchange data.

Here is an example session with kdb (source: [TutorialsPoint](https://www.tutorialspoint.com/kdbplus/kdbplus_quick_guide.htm)):

```q
q)trade:([]sym:5?`ibm`msft`hsbc`samsung;price:5?(303.00*3+1);size:5?(900*5);time:5?(.z.T-365))

q)trade

  sym        price   size   time
-----------------------------------------
 msft      743.8592  3162  02:32:17.036
 msft      641.7307  2917  01:44:56.936
 hsbc      838.2311  1492  00:25:23.210
 samsung   278.3498  1983  00:29:38.945
 ibm       838.6471  4006  07:24:26.842
```

The first thing you might notice is that it is *very* different from your usual SQL. The language one'd use to talk to kdb, in fact, is **q**, the language kdb itself is written in.

So now it's the perfect time to break down the two statements.

The first statement, essentially creates a list, or a table.

The second statement, simply calls the table, which is named `trade`.
